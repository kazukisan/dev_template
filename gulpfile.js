var gulp = require('gulp');
var config = require('./config');
var requireDir = require('require-dir');
var runSequence = require('run-sequence').use(gulp);

// tasksディレクトリのタスク読み込み
requireDir('./tasks/');

// build
gulp.task('build', ['clean'], () => {
	config.isBuild = true;
	runSequence(config.build.tasks);
});

// Production
gulp.task('production', ['clean'], () => {
	config.isProduction = true;
	runSequence(config.production.tasks);
});

// default
gulp.task('default', () => {
	runSequence(config.default.tasks);
});
