'use strict';


let path = {
	src: 'src/',
	dest: 'dest/'
};

let $ = require('./tasks/plugins');


module.exports = {
	path: path,
	isBuild: false,
	isProduction: false,


///////////////////////////////////////////////////////////
// tasks
//

	/* default
	-----------------------------------------------------*/
	default: {
		tasks: [
			'server',
			'watch'
		]
	},


	/* build
	-----------------------------------------------------*/
	build: {
		tasks: [
			'copy',
			'html',
			'ejs',
			'sass',
			'vendor',
			'eslint',
			'js',
			'imagemin'
		]
	},


	/* production
	-----------------------------------------------------*/
	production: {
		tasks: [
			'copy',
			'html',
			'ejs',
			'sass',
			'vendor',
			'eslint',
			'js',
			'imagemin'
		]
	},





///////////////////////////////////////////////////////////
// default
//

	/* browserSync
	-----------------------------------------------------*/
	browserSync: {
		// proxy: "_pathToDir_.localhost"
		server: {
			baseDir: path.dest
		}
	},


	/* del
	-----------------------------------------------------*/
	del: {
		src: [
			path.dest + '/**/*',
			'!' + path.dest + '/.git/**',
			'!' + path.dest + '**/lacne/**',
			'!' + path.dest + '**/phpMyAdmin/**',
			'!' + path.dest + '**/phpmyadmin/**'
		]
	},


	/* html
	-----------------------------------------------------*/
	html: {
		src: [
			path.src + '**/!(_)*.+(html|shtml|htm)'
		],
		minifyOptions: {
			collapseWhitespace: true
		}
	},


	/* ejs
	-----------------------------------------------------*/
	ejs: {
		src: [
			path.src + '**/!(_)*.ejs'
		]
	},


	/* sass
	-----------------------------------------------------*/
	sass: {
		src: [
			path.src + '**/scss/**/*.scss'
		],
		options: {
			outputStyle: 'expanded',
			includePaths: $.bourbon.includePaths
		}
	},


	/* js
	-----------------------------------------------------*/
	js: {
		src: [
			path.src + '**/js/**/*.js',
			'!' + path.src + '**/lib/**/*.js',
			'!' + path.src + '**/libs/**/*.js',
			'!' + path.src + '**/module/**/*.js',
			'!' + path.src + '**/modules/**/*.js'
		],
		uglifyOptions: {
			preserveComments: 'license'
		}
	},


	/* pleeese
	-----------------------------------------------------*/
	pleeease: {
		options: {
			build: {
				browsers: ["last 2 versions", "ie >= 9", "Android >= 4","ios_saf >= 8"],
				sourcemaps: false,
				mqpacker: false,
				minifier: false,
				import: false
			},
			production: {
				browsers: ["last 2 versions", "ie >= 9", "Android >= 4","ios_saf >= 8"],
				sourcemaps: false,
				mqpacker: false,
				minifier: false,
				import: false
			}
		}
	},


	/* imagemin
	-----------------------------------------------------*/
	imagemin: {
		src: path.src + '**/img/**/*.+(jpg|jpeg|png|gif)',
		options: {
			progressive: true,
			use: [
				$.pngquant({
					quality: '80-90',
					speed: 1
				}),
				$.jpegtran()
			]
		}
	},


	/* vendor
	-----------------------------------------------------*/
	vendor: {
		src: [
			'bower_components/jquery/dist/jquery.min.js',
			'bower_components/easing/easing-min.js',
			'bower_components/gsap/src/minified/TweenMax.min.js',
			'bower_components/EaselJS/lib/easeljs-0.8.2.min.js',
			'bower_components/imagesloaded/imagesloaded.pkgd.min.js',
			'bower_components/underscore/underscore-min.js'
		],
		filename: 'vendor.js',
		dest: path.dest + 'assets/js/'
	},


	/* copy
	-----------------------------------------------------*/
	copy: {
		src: [
			path.src + '**/*.md',
			path.src + '**/*.txt',
			path.src + '**/*.php',
			path.src + '**/*.inc',
			path.src + '**/*.tpl',
			path.src + '**/*.xml',
			path.src + '**/*.json',
			path.src + '**/*.ico',
			path.src + '**/*.swf',
			path.src + '**/*.pdf',
			path.src + '**/*.mp3',
			path.src + '**/*.mp4',
			path.src + '**/*.ogv',
			path.src + '**/*.webm',
			path.src + '**/*.zip',
			path.src + '**/*.svg',
			path.src + '**/*.otf',
			path.src + '**/*.ttf',
			path.src + '**/*.eot',
			path.src + '**/*.woff',
			path.src + '**/*.woff2',
			path.src + '.htaccess',
			'!' + path.src + '**/lacne/**',
			'!' + path.src + '**/phpMyAdmin/**',
			'!' + path.src + '**/phpmyadmin/**'

		]
	},


	/* concat
	-----------------------------------------------------*/
	concat: [
		{
			type: 'css/js',
			filename: 'new_file.css/js',
			filepath: [
				path.src + 'path_to_file',
				path.src + 'path_to_file',
				path.src + 'path_to_file'
			],
			dest: path.dest + 'path_to_dir'
		}
	]
};
