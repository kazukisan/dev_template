import controller from './_controller';
import domCollection from './_domObjectCollection';
import pageState from './_pageState.js';
import EVENTS from './_events.js';

const modeClass = {
	pc: 'mode-pc',
	sp: 'mode-sp'
};

/**
 * @class PageView
 */
export default class PageView {

	/**
	 * 初期化
	 *
	 * @constructor
	 */
	constructor() {
		this._$window   = domCollection.get('window');
		this._$document = domCollection.get('document');
		this._$html     = domCollection.get('html');
		this._$head     = domCollection.get('head');
		this._$page     = domCollection.get('#page');
		this._init = true;

		this._handleEvents();

		this._init();
	}


	_init() {
		this._getWindowSize();
		this._checkMode();

		this._init = false;
	}



	/**
	 * イベントの監視
	 */
	_handleEvents() {
		// resize
		this._$window.on(EVENTS.RESIZE, _.debounce(this._onResize.bind(this), 50));
		// scroll
		this._$window.on(EVENTS.SCROLL, _.throttle(this._onScroll.bind(this), 10));
		// load
		controller.on(EVENTS.LOAD, this._getScrollBarWidth.bind(this));
	}



	/**
	 * リサイズ処理
	 *
	 * - PageStateインスタンスの値を更新
	 * - checkMode関数の呼び出し
	 * - リサイズイベントの通知
	 */
	_onResize() {
		this._getWindowSize();
		this._checkMode();

		controller.dispatchEvent(EVENTS.RESIZE);
	}



	/**
	 * ブラウザのサイズを取得
	 */
	_getWindowSize() {
		pageState.set('windowSize', {
			width: this._$window.width(),
			height: this._$window.height()
		});
	}



	/**
	 * スクロールイベント発行
	 */
	_onScroll() {
		let scrollValue = this._$document.scrollTop();

		controller.dispatchEvent({ type: EVENTS.SCROLL, data: { scroll: scrollValue } });
	}



	/**
	 * スクロールイベント発行
	 */
	_onScrollPage() {
		let scrollValue = this._$page.scrollTop();

		controller.dispatchEvent({ type: EVENTS.SCROLL, data: { scroll: scrollValue } });
	}



	/**
	 * 表示モードの判定
	 *
	 * - PageStateインスタンスの値を更新
	 * - デバイス専用イベントと変更イベントの通知
	 */
	_checkMode() {
		let mode        = this._$head.css('font-family').replace(/"/gi, '');
		let currentMode = pageState.get('mode');

		// 専用イベントの発行
		if ( currentMode !== mode ) {
			// state変更
			pageState.set('mode', mode);
			pageState.set('isPC', ( mode === 'pc' ) );
			pageState.set('isSP', ( mode === 'sp' ) );

			// htmlに該当クラスを付与
			this._$html.removeClass(modeClass[currentMode]);
			this._$html.addClass(modeClass[mode]);

			// スクロールバーサイズ更新
			this._getScrollBarWidth();

			// イベントの発行
			if ( !this._init ) {
				controller.dispatchEvent({
					type: EVENTS.MODECHANGE,
					data: { mode: mode }
				});
			}
		}
	}



	/**
	 * スクロールバーのサイズを取得
	 */
	_getScrollBarWidth() {
		let scrollBarWidth = window.innerWidth - $(window).innerWidth();

		pageState.set('scrollBarWidth', scrollBarWidth);
	}
}
