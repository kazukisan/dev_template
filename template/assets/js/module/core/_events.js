export default {
	LOAD: 'load',
	MODECHANGE: 'modechange',
	SCROLL: 'scroll',
	RESIZE: 'resize',
	STATECHANGE_MODE: 'statechange:mode'
};
