import controller from './_controller.js';

/**
 * @class PageState
 */
class PageState {

	/**
	 * 初期化
	 *
	 * @constructor
	 */
	constructor() {
		this._state = {};
	}



	/**
	 * 指定されたプロパティ名の値の存在をチェック
	 *
	 * @param  {String}  name プロパティのキー名
	 * @return {Boolean}      真偽値を返す
	 */
	has(name) {
		return !!this._state[name];
	}



	/**
	 * 指定されたプロパティ名の値を返す
	 * @param  {String} name プロパティのキー名
	 * @return {[type]}      該当無しの場合はfalse, 有りの場合は値を返す
	 */
	get(name) {
		if ( !this._state[name] ) {
			return false;
		} else {
			return this._state[name];
		}
	}



	/**
	 * 指定されたプロパティ名に値をセットする
	 * @param {String} name  プロパティのキー名
	 * @param {[type]} value セットする値
	 */
	set(name, value) {
		this._state[name] = value;

		this._dispatchEvent(name);
	}


	_dispatchEvent(name) {
		controller.dispatchEvent({
			type: 'statechange:' + name,
			data: {
				state: this.get(name)
			}
		});
	}
}



/*--------------------------------------------------------------------------
   export
---------------------------------------------------------------------------*/
export default new PageState;
