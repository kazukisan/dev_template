/**
 * @class DomObjectCollection
 */
class DomObjectCollection {
	constructor() {
		this._$objects = {};

		this._$objects.window   = $(window);
		this._$objects.document = $(document);
		this._$objects.html     = $('html');
		this._$objects.head     = $('head');
		this._$objects.body     = $('body');
	}

	get(selector) {
		if ( !this._$objects[selector] ) {
			return this._$objects[selector] = $(selector);
		} else {
			return this._$objects[selector];
		}
	}

	has(selector) {
		return !!this._$objects[selector];
	}
}



/*--------------------------------------------------------------------------
   export
---------------------------------------------------------------------------*/
export default new DomObjectCollection;
