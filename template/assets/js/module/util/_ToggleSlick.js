import pageState from '../core/_pageState.js';
import controller from '../core/_controller.js';
import 'slick';

export default class ToggleSlick {
	constructor($group, options) {
		this._$group = $group;
		this._options = options;

		this._onModechange();
		this._handleEvents();
	}

	_handleEvents() {
		controller.on('modechange', this._onModechange.bind(this));
	}

	_onModechange(e) {
		let mode = (e) ? e.data.mode: pageState.get('mode');
		let $group = this._$group;
		let options = this._options;

		if ( options[mode] ) {
			$group.slick(options[mode]);
			this._flg = true;
		} else if ( this._flg ) {
			$group.slick('unslick');
		}
	}
}
