import controller from '../core/_controller';
import domCollection from '../core/_domObjectCollection';
import pageState from '../core/_pageState';
import EVENTS from '../core/_events.js';

export default class FlatHeight {
	constructor(selector, split) {
		this._$elem = domCollection.get(selector);

		this._split = {
			pc: (split.pc) ? split.pc : this._$elem.length,
			sp: (split.sp) ? split.sp : this._$elem.length
		};

		this._onResize();
		this._handleEvents();
	}


	_handleEvents() {
		controller.on(EVENTS.RESIZE, this._onResize.bind(this));
		controller.on(EVENTS.LOAD, this._onResize.bind(this));
	}


	_splitElem() {
		let i, j, pointer, $splitGroup, heightAry, max;
		let len = ( pageState.get('isPC') )
			? Math.ceil(this._$elem.length / this._split.pc)
			: Math.ceil(this._$elem.length / this._split.sp);
		let currentsplit = ( pageState.get('isPC') )
			? this._split.pc
			: this._split.sp;

		for ( i = 0, pointer = 0; i < len; i++ ) {
			$splitGroup = [];
			heightAry = [];

			for ( j = 0; j < currentsplit; j++, pointer++ ) {
				$splitGroup[j] = this._$elem.eq(pointer);
				heightAry[j] = $splitGroup[j].outerHeight();
			}

			max = Math.max.apply(null, heightAry);

			this._flatten($splitGroup, max);
		}
	}


	_flatten($splitGroup, max) {
		var i, len = $splitGroup.length;

		for ( i = 0; i < len; i++ ) {
			$splitGroup[i].outerHeight(max);
		}
	}


	_reset() {
		this._$elem.css('height', '');
	}


	_onResize() {
		let mode = pageState.get('mode');
		this._reset();

		if ( this._split[mode] > 1 ) {
			this._splitElem();
		}
	}
}
