import controller from '../core/_controller.js';
import domCollection from '../core/_domObjectCollection';
import EVENTS from '../core/_events.js';

export default (selector, bgEnable) => {
	let $elem = domCollection.get(selector);
	let backgroundEnable = (bgEnable) ? true : false;

	$elem.imagesLoaded({ background: backgroundEnable }, () => {
		controller.dispatchEvent(EVENTS.LOAD);
	});
};
