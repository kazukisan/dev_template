import domCollection from '../core/_domObjectCollection';
import 'scrollToPlugin';

let config = {
	speed: 1.2,
	ease: Power3.easeOut
};

export default class Scroller {
	constructor(triggers, parent, options) {
		this._$parent = domCollection.get(parent);
		this._$triggers = domCollection.get(triggers);
		this._options = $.extend({}, config, options);
		this._isDefault = ( parent === window ) ? true : false;
		this._handleEvents();
	}

	_handleEvents() {
		this._$triggers.on('click', this._onClick.bind(this));
	}

	_onClick(e) {
		e.preventDefault();
		let $trigger = $(e.currentTarget);
		let data = $trigger.data('target');
		let $target = ( data ) ? $(data) : $($trigger.attr('href'));

		let offset = ( this._isDefault ) ? $target.offset().top : $target.position().top + this._$parent.scrollTop();

		TweenMax.to(this._$parent, this._options.speed, {
			scrollTo: { y: offset, autoKill: false },
			ease: this._options.ease
		});
	}
}
