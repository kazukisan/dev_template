'use strict';

var path = require('path');
var webpack = require('webpack');

module.exports = {
	watch: true,
	module: {
		loaders: [{
			test: /\.js$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'babel-loader',
			query:{
				presets: ['es2015']
			}
		}]
	},
	externals: {
		TweenLite: 'TweenLite'
	},
	resolveLoader: {
		root: path.join(__dirname, 'node_modules')
	},
	plugins: [
		new webpack.ResolverPlugin(
			new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
		),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.ProvidePlugin({
			// jQuery: 'jquery',
			// $: 'jquery',
			// jquery: 'jquery',
			// createjs: 'EaselJS',
			// underscore: 'underscore',
			// _: 'underscore'
		})
	],
	resolve: {
		extensions: ['', '.js'],
		root: [path.join(__dirname, "bower_components")],
		modulesDirectories: ['node_modules', 'bower_components'],
		alias: {
			bower: __dirname + '/bower_components',
			jquery: __dirname + '/bower_components/jquery/dist/jquery.min.js',
			TweenMax: __dirname + '/bower_components/gsap/src/minified/TweenMax.min.js',
			scrollToPlugin: __dirname + '/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js',
			EaselJS: __dirname + '/bower_components/EaselJS/lib/easeljs-0.8.2.min.js',
			imagesLoaded: __dirname + '/bower_components/imagesloaded/imagesloaded.pkgd.min.js',
			underscore: __dirname + '/bower_components/underscore/underscore-min.js',
			slick: __dirname + '/bower_components/slick-carousel/slick/slick.min.js'
		}
	}
};
