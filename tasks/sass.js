'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('sass', () => {
	return gulp.src(config.sass.src)
		.pipe($.plumber({
			errorHandler: $.notify.onError("Error: <%= error.message %>")
		}))
		.pipe($.cached('sass'))
		.pipe($.dependents())
		.pipe($.if(config.isBuild, $.sourcemaps.init()))
		.pipe($.sass(config.sass.options))
		.pipe($.if(
			config.isProduction,
			$.pleeease(config.pleeease.options.production),
			$.pleeease(config.pleeease.options.build)
		))
		.pipe($.csscomb())
		.pipe($.if(config.isProduction, $.cssmin()))
		.pipe($.if(config.isBuild, $.sourcemaps.write('./maps')))
		.pipe($.rename( (path) => {
			var newPath = path.dirname;
			path.dirname = newPath.replace(/scss/g, 'css');
			return path;
		}))
		.pipe(gulp.dest(config.path.dest))
		.pipe($.browserSync.stream());
});
