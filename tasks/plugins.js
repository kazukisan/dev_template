'use strict';

let loader = require('gulp-load-plugins');
let pngquant = require('imagemin-pngquant');
let jpegtran = require('imagemin-jpegtran');
let bourbon = require('node-bourbon');
let browserSync = require('browser-sync').create();

let $ = loader({
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /^gulp(-|\.)/
});
$.pngquant = pngquant;
$.jpegtran = jpegtran;
$.browserSync = browserSync;
$.bourbon = bourbon;

module.exports = $;
