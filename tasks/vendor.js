'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('vendor', () => {
	return gulp.src(config.vendor.src)
		.pipe($.concat(config.vendor.filename))
		.pipe($.uglify(config.js.uglifyOptions))
		.pipe(gulp.dest(config.vendor.dest));
});
