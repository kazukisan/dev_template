'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('copy', () => {
	return gulp.src(config.copy.src)
		.pipe($.changed(config.path.dest))
		.pipe(gulp.dest(config.path.dest))
		.pipe($.browserSync.stream());
});
