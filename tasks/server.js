'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('server', () => {
	return $.browserSync.init(config.browserSync);
});
