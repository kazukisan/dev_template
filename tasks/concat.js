'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('concat', (callback) => {
	let len = config.concat.length;
	let i = 0;

	if ( config.isProduction ) {
		for ( i = 0; i < len; i++ ) {
			gulp.src(config.concat[i].filepath)
				.pipe($.plumber({
					errorHandler: $.notify.onError("Error: <%= error.message %>")
				}))
				.pipe($.concat(config.concat[i].filename))
				.pipe($.if(config.concat[i].type === 'js', $.uglify(config.js.uglifyOptions)))
				.pipe($.if(config.concat[i].type === 'css', $.cssmin()))
				.pipe(gulp.dest(config.concat[i].dest))
				.on('end', () => { onEnd(len, i, callback); });
		}
	} else {
		for ( i = 0; i < len; i++ ) {
			gulp.src(config.concat[i].filepath)
				.pipe($.plumber({
					errorHandler: $.notify.onError("Error: <%= error.message %>")
				}))
				.pipe($.concat(config.concat[i].filename))
				.pipe(gulp.dest(config.concat[i].dest))
				.on('end', () => { onEnd(len, i, callback); });
		}
	}
});

function onEnd(len, i, cb) {
	if (len === i + 1) {
		cb();
	}
}
