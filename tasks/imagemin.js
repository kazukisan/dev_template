'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('imagemin', () => {
	return gulp.src(config.imagemin.src)
		.pipe($.plumber())
		.pipe($.changed(config.path.dest))
		.pipe($.if(config.isProduction, $.imagemin(config.imagemin.options)))
		.pipe(gulp.dest(config.path.dest))
		.pipe($.browserSync.stream());
});
