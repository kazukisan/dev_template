'use strict';

let gulp = require('gulp');
let del = require('del');
let config = require('../config');

gulp.task('clean', () => {
	return del.sync(config.del.src, {
		force: true,
		dot: true
	});
});
