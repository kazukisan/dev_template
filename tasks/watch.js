'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('watch', () => {

	// html
	$.watch(config.html.src, () => {
		return gulp.start('html');
	});

	// ejs
	$.watch(config.ejs.src, () => {
		return gulp.start('ejs');
	});

	// sass
	$.watch(config.sass.src, () => {
		return gulp.start('sass');
	});

	// eslint
	$.watch(config.js.src, () => {
		return gulp.start('eslint');
	});

	// js
	$.watch(config.js.src, () => {
		return gulp.start('js');
	});

	// copy
	$.watch(config.copy.src, () => {
		return gulp.start('copy');
	});

	// imagemin
	$.watch(config.imagemin.src, () => {
		return gulp.start('imagemin');
	});
});
