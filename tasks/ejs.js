'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('ejs', () => {
	return gulp.src(config.ejs.src)
		.pipe($.plumber())
		.pipe($.cached('ejs'))
		.pipe($.ejs(config.ejs.options))
		.pipe($.if(config.isProduction, $.htmlmin(config.html.minifyOptions)))
		.pipe(gulp.dest(config.path.dest))
		.pipe($.browserSync.stream());
});
