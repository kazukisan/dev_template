'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('html', () => {
	return gulp.src(config.html.src)
		.pipe($.plumber())
		.pipe($.cached('html'))
		.pipe($.if(config.isProduction, $.htmlmin(config.html.minifyOptions)))
		.pipe(gulp.dest(config.path.dest))
		.pipe($.browserSync.stream());
});
