'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');

gulp.task('eslint', () => {
	return gulp.src(config.js.src)
		.pipe($.plumber({
			errorHandler: $.notify.onError("Error: <%= error.message %>")
		}))
		.pipe($.cached('eslint'))
		.pipe($.eslint({ useEslintrc: true }))
		.pipe($.eslint.format())
		.pipe($.eslint.failOnError())
		.pipe($.plumber.stop());
});
