'use strict';

let gulp = require('gulp');
let config = require('../config');
let $ = require('./plugins');
let named = require('vinyl-named');
let webpackConfig = require('../webpack.config');

gulp.task('js', () => {
	if ( config.isBuild || config.isProduction ) webpackConfig.watch = false;
	return gulp.src(config.js.src)
		.pipe($.plumber())
		.pipe(named((file) => {
			return file.relative.replace(/\.[^\.]+$/, '');
		}))
		.pipe($.webpack(webpackConfig))
		.pipe($.if(config.isProduction, $.uglify(config.js.uglifyOptions)))
		.pipe(gulp.dest(config.path.dest))
		.pipe($.browserSync.stream());
});
